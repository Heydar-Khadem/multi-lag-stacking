﻿Please, cite the following paper in case you find this repository useful for your research:

Heydar Khadem, Hoda Nemat, Jackie Elliott and Mohammed Benaissa, "Multi-lag Stacking for Blood Glucose Level Prediction," The 5th International Workshop on Knowledge Discovery in Healthcare Data, Santiago de Compostela, Spain, August 30, 2020.

----------------------------------

Requirements:

Python ≥ 3.6, TensorFlow ≥ 1.15.0, Keras ≥ 2.2.5, Pandas, NumPy and Sklearn

--------------------------------

Instruction to run the codes:

Access to the Ohio dataset is required to reproduce the results.

Run the 'xml2csv.py' file to extract blood glucose data from XML files and save them in CSV files. Note that the XML path needs is the path on disk where the to the folder containing the XML files for the Ohio dataset.

Run 'data_preparing'.py to take care of missing data, and translating the time series problem to a supervised learning task.

Run 'systems.py' to implement the prediction systems. RMSE, MAE and the raw results will be automatically saved.
