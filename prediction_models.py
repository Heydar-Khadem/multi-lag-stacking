"""
Created on Mar 2020

@authors: Heydar Khadem (h.khadem@sheffiel.ac.uk) and Hoda Nemat (hoda.nemat@sheffield.ac.uk)

Please cite the following work if you find this code useful for your research:
    
"Heydar Khadem, Hoda Nemat, Jackie Elliott and Mohammed Benaissa,
Multi-lag Stacking for Blood Glucose Level Prediction,
The 5th International Workshop on Knowledge Discovery in Healthcare Data,
Santiago de Compostela, Spain, August 30, 2020"

"""


from keras.models import Sequential
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error 
from sys import stdout
import numpy as np
from sklearn.cross_decomposition import PLSRegression
from keras.layers import Dense, LSTM






def MLP_model(Train_X, Train_Y, Test_X, Test_Y, epochs=100):
    
    model = Sequential()
    model.add(Dense(100, activation='relu', input_dim=Train_X.shape[1]))
    model.add(Dense(Train_Y.shape[1]))
    model.compile(optimizer='adam', loss='mse')
    model.fit(Train_X, Train_Y,  epochs=epochs, verbose=0)
    
    Predict_Y = model.predict(Test_X, verbose=0)
    Y_pred_train = model.predict(Train_X, verbose=0)

    MSE = mean_squared_error(Test_Y, Predict_Y, multioutput='raw_values')
    RMSE = MSE** 0.5
    MAE = mean_absolute_error(Test_Y, Predict_Y, multioutput='raw_values')

            
    return   Predict_Y, model, list(RMSE), Y_pred_train, list(MAE)
    


def LSTM_model(Train_X, Train_Y, Test_X, Test_Y, epochs=100):
    
    Train_X = Train_X.reshape(Train_X.shape[0], Train_X.shape[1], 1)
    Test_X = Test_X.reshape(Test_X.shape[0], Test_X.shape[1], 1)
    n_timesteps, n_features = Train_X.shape[1], Train_X.shape[2]
    

    model = Sequential()
    model.add(LSTM(200, activation='relu', input_shape=(n_timesteps, n_features)))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(Train_Y.shape[1]))
    model.compile(optimizer='adam', loss='mse')
    model.fit(Train_X, Train_Y, epochs=epochs, verbose=0)

    Predict_Y = model.predict(Test_X, verbose=0)
    Y_pred_train = model.predict(Train_X, verbose=0)

    MSE = mean_squared_error(Test_Y, Predict_Y, multioutput='raw_values')
    RMSE = MSE** 0.5
    MAE = mean_absolute_error(Test_Y, Predict_Y, multioutput='raw_values')
        
  
    return   Predict_Y, model, list(RMSE), Y_pred_train, list(MAE)


def PLSR_model (Train_X, Train_Y, Test_X, Test_Y):
    
    data_size = len(Train_X[:, 0])
    

    cni = [] 
    rmse = []
    r2 = []

    componentmax = Train_X.shape[1]
    component = np.arange(1, componentmax)
    for i in component:
        pls = PLSRegression(n_components=i)
        # Fit
        pls.fit(Train_X, Train_Y )
        # Prediction
        Y_pred_train = pls.predict(Train_X)
     
        msecv = mean_squared_error(Train_Y , Y_pred_train)
        PRESS = msecv*data_size
        cni_i = PRESS/(data_size-i-1)
        cni.append(cni_i)
        
        rmsecv = np.sqrt(msecv)
        rmse.append(rmsecv)
        r2_p = r2_score(Train_Y , Y_pred_train)
        r2.append (r2_p)
    
        comp = 100*(i+1)/componentmax
        
        stdout.write("\r%d%% completed" % comp)
        stdout.flush()
    stdout.write("\n")
    
    msemin = np.argmin(rmse)
    
    stdout.write("\n")
    
    
    n_components=msemin+1
    
    model = PLSRegression(n_components)
    model.fit(Train_X, Train_Y)
    
    Predict_Y = model.predict(Test_X)
    Y_pred_train = model.predict(Train_X)
    
    
    
    MSE = mean_squared_error(Test_Y, Predict_Y, multioutput='raw_values')
    RMSE = MSE** 0.5
    MAE = mean_absolute_error(Test_Y, Predict_Y, multioutput='raw_values')
      
    return   Predict_Y, model, list(RMSE), Y_pred_train, list(MAE)