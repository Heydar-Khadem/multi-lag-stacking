"""
Created on Mar 2020

@authors: Heydar Khadem (h.khadem@sheffiel.ac.uk) and Hoda Nemat (hoda.nemat@sheffield.ac.uk)

Please cite the following work if you find this code useful for your research:
    
"Heydar Khadem, Hoda Nemat, Jackie Elliott and Mohammed Benaissa,
Multi-lag Stacking for Blood Glucose Level Prediction,
The 5th International Workshop on Knowledge Discovery in Healthcare Data,
Santiago de Compostela, Spain, August 30, 2020"

"""

import pandas as pd
from pandas import read_csv
import numpy as np
import pickle as pickle
from prediction_models import MLP_model, LSTM_model, PLSR_model




def calc_result(PID, horizon):

        
    with open('TrainTestXY_'+ PID +'_history6_horizon' + horizon + '.pkl', 'rb') as f:
        Train_X_history6, Train_Y, Test_X_history6, Test_Y, _, _, _, _ = pickle.load(f)   
    
    with open('TrainTestXY_'+ PID +'_history12_horizon' + horizon + '.pkl', 'rb') as f:
        Train_X_history12, Train_Y, Test_X_history12, Test_Y, _, _, _, _ = pickle.load(f)  
        
    d={}
     
    d['Train_X_history6'] = Train_X_history6
    d['Train_X_history12'] = Train_X_history12   
    
    
    d['Train_Y'] = Train_Y
    
    
    d['Test_X_history6'] = Test_X_history6 
    d['Test_X_history12'] = Test_X_history12
    d['Test_Y'] = Test_Y 
    ########## raw data ######################    
    
    Original_test = read_csv('glucose_value_' + PID +'_testing.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
    Evaluation_test = Original_test[12:]
    
    Result_SYS1 = pd.DataFrame({'TimeStamp':Evaluation_test.index,'Y_True':Evaluation_test.values},index=range(1,len(Evaluation_test)+1))
    Result_SYS2 = pd.DataFrame({'TimeStamp':Evaluation_test.index,'Y_True':Evaluation_test.values},index=range(1,len(Evaluation_test)+1))
    Result_SYS3 = pd.DataFrame({'TimeStamp':Evaluation_test.index,'Y_True':Evaluation_test.values},index=range(1,len(Evaluation_test)+1))
    
    
    RMSE_basic = pd.DataFrame({})
    MAE_basic = pd.DataFrame({})
    
    
    RMSE_stacked = pd.DataFrame({})
    MAE_stacked = pd.DataFrame({})    
    
     
     
    RUN = 5
    for i in range(1,RUN+1):
        Models = ['history6', 'history12']   
        
        
        d['RMSE_basic_run{}'.format(i)] = pd.DataFrame({}, index=['run{}'.format(i)])
        d['MAE_basic_run{}'.format(i)] = pd.DataFrame({}, index=['run{}'.format(i)])
        
        
        d['RMSE_stacked_run{}'.format(i)] = pd.DataFrame({}, index=['run{}'.format(i)])
        d['MAE_stacked_run{}'.format(i)] = pd.DataFrame({}, index=['run{}'.format(i)])
        
        # Basic models
        for k in range(len(Models)):
            
            d['Y_pred_plsr_{}_run{}'.format(Models[k], i)], d['model_plsr_{}_run{}'.format(Models[k], i)], d['RMSE_plsr_{}_run{}'.format(Models[k], i)], d['Y_pred_plsr_train_{}_run{}'.format(Models[k], i)], d['MAE_plsr_{}_run{}'.format(Models[k], i)] = PLSR_model(d['Train_X_{}'.format(Models[k])], d['Train_Y'], d['Test_X_{}'.format(Models[k])], d['Test_Y'])
            d['Y_pred_mlp_{}_run{}'.format(Models[k], i)], d['model_mlp_{}_run{}'.format(Models[k], i)], d['RMSE_mlp_{}_run{}'.format(Models[k], i)], d['Y_pred_mlp_train_{}_run{}'.format(Models[k], i)], d['MAE_mlp_{}_run{}'.format(Models[k], i)] = MLP_model(d['Train_X_{}'.format(Models[k])], d['Train_Y'], d['Test_X_{}'.format(Models[k])], d['Test_Y'])
            d['Y_pred_lstm_{}_run{}'.format(Models[k], i)], d['model_lstm_{}_run{}'.format(Models[k], i)], d['RMSE_lstm_{}_run{}'.format(Models[k], i)], d['Y_pred_lstm_train_{}_run{}'.format(Models[k], i)], d['MAE_lstm_{}_run{}'.format(Models[k], i)] = LSTM_model(d['Train_X_{}'.format(Models[k])], d['Train_Y'], d['Test_X_{}'.format(Models[k])], d['Test_Y'])
    
            d['RMSE_basic_run{}'.format(i)]['RMSE_plsr_{}'.format(Models[k])] = d['RMSE_plsr_{}_run{}'.format(Models[k], i)][-1]
            d['RMSE_basic_run{}'.format(i)]['RMSE_mlp_{}'.format(Models[k])] = d['RMSE_mlp_{}_run{}'.format(Models[k], i)][-1]
            d['RMSE_basic_run{}'.format(i)]['RMSE_lstm_{}'.format(Models[k])] = d['RMSE_lstm_{}_run{}'.format(Models[k], i)][-1]
          
        
            d['MAE_basic_run{}'.format(i)]['MAE_plsr_{}'.format(Models[k])] = d['MAE_plsr_{}_run{}'.format(Models[k], i)][-1]
            d['MAE_basic_run{}'.format(i)]['MAE_mlp_{}'.format(Models[k])] = d['MAE_mlp_{}_run{}'.format(Models[k], i)][-1]
            d['MAE_basic_run{}'.format(i)]['MAE_lstm_{}'.format(Models[k])] = d['MAE_lstm_{}_run{}'.format(Models[k], i)][-1]
            
      
        d['Train_X_stacked_SYS1_run{}'.format(i)] = np.concatenate((d['Y_pred_mlp_train_history6_run{}'.format(i)], d['Y_pred_plsr_train_history6_run{}'.format(i)], d['Y_pred_lstm_train_history6_run{}'.format(i)]), axis = 1)
        d['Test_X_stacked_SYS1_run{}'.format(i)] = np.concatenate((d['Y_pred_mlp_history6_run{}'.format(i)], d['Y_pred_plsr_history6_run{}'.format(i)], d['Y_pred_lstm_history6_run{}'.format(i)]), axis = 1)
        
        d['Train_X_stacked_SYS2_run{}'.format(i)] = np.concatenate((d['Y_pred_mlp_train_history12_run{}'.format(i)], d['Y_pred_plsr_train_history12_run{}'.format(i)], d['Y_pred_lstm_train_history12_run{}'.format(i)]), axis = 1)
        d['Test_X_stacked_SYS2_run{}'.format(i)] = np.concatenate((d['Y_pred_mlp_history12_run{}'.format(i)], d['Y_pred_plsr_history12_run{}'.format(i)], d['Y_pred_lstm_history12_run{}'.format(i)]), axis = 1)        
     
        d['Train_X_stacked_SYS3_run{}'.format(i)] = np.concatenate((d['Y_pred_mlp_train_history6_run{}'.format(i)], d['Y_pred_plsr_train_history6_run{}'.format(i)], d['Y_pred_lstm_train_history6_run{}'.format(i)],d['Y_pred_mlp_train_history12_run{}'.format(i)], d['Y_pred_plsr_train_history12_run{}'.format(i)], d['Y_pred_lstm_train_history12_run{}'.format(i)]), axis = 1)
        d['Test_X_stacked_SYS3_run{}'.format(i)] = np.concatenate((d['Y_pred_mlp_history6_run{}'.format(i)], d['Y_pred_plsr_history6_run{}'.format(i)], d['Y_pred_lstm_history6_run{}'.format(i)],d['Y_pred_mlp_history12_run{}'.format(i)], d['Y_pred_plsr_history12_run{}'.format(i)], d['Y_pred_lstm_history12_run{}'.format(i)]), axis = 1)        
        
        
        # meta learner
        stacked_methods = ['SYS1','SYS2','SYS3']
    
        for k in range(len(stacked_methods)):
            
            d['yhat_stacked_{}_run{}'.format(stacked_methods[k], i)], d['model_stacked_{}_run{}'.format(stacked_methods[k], i)], d['RMSE_stacked_{}_run{}'.format(stacked_methods[k], i)], d['Y_pred_stacked_train_{}_run{}'.format(stacked_methods[k], i)], d['MAE_stacked_{}_run{}'.format(stacked_methods[k], i)] = PLSR_model(d['Train_X_stacked_{}_run{}'.format(stacked_methods[k], i)], d['Train_Y'], d['Test_X_stacked_{}_run{}'.format(stacked_methods[k], i)], d['Test_Y'])
            
            
            
            # raw prediction
            Result_SYS1['yhat_SYS1_run{}'.format(i)]=d['yhat_stacked_{}_run{}'.format(stacked_methods[k], i)][:,-1]
            Result_SYS2['yhat_SYS2_run{}'.format(i)]=d['yhat_stacked_{}_run{}'.format(stacked_methods[k], i)][:,-1]
            Result_SYS3['yhat_SYS3_run{}'.format(i)]=d['yhat_stacked_{}_run{}'.format(stacked_methods[k], i)][:,-1]

            
            
            d['RMSE_stacked_run{}'.format(i)]['RMSE_stacked_{}'.format(stacked_methods[k])] = d['RMSE_stacked_{}_run{}'.format(stacked_methods[k], i)][-1]
            d['MAE_stacked_run{}'.format(i)]['MAE_stacked_{}'.format(stacked_methods[k])] = d['MAE_stacked_{}_run{}'.format(stacked_methods[k], i)][-1]
            
            
      
        RMSE_basic = RMSE_basic.append(d['RMSE_basic_run{}'.format(i)]) 
        MAE_basic = MAE_basic.append(d['MAE_basic_run{}'.format(i)]) 
            
        RMSE_stacked = RMSE_stacked.append(d['RMSE_stacked_run{}'.format(i)]) 
        MAE_stacked = MAE_stacked.append(d['MAE_stacked_run{}'.format(i)])
        
    # calculating mean and standard deviation over runs    
    RMSE_basic.loc['AvgRuns'] = RMSE_basic.mean()
    MAE_basic.loc['AvgRuns'] = MAE_basic.mean()
    RMSE_stacked.loc['AvgRuns'] = RMSE_stacked.mean()
    MAE_stacked.loc['AvgRuns'] = MAE_stacked.mean()
    
    RMSE_basic.loc['StdRuns'] = RMSE_basic.std()
    MAE_basic.loc['StdRuns'] = MAE_basic.std()
    RMSE_stacked.loc['StdRuns'] = RMSE_stacked.std()
    MAE_stacked.loc['StdRuns'] = MAE_stacked.std()
    
    
    d['RMSE_basic'] = RMSE_basic
    d['MAE_basic'] = MAE_basic
    d['RMSE_stacked'] = RMSE_stacked
    d['MAE_stacked'] = MAE_stacked
    
    
    # saving RMSE and MAE
    RMSE_basic.to_csv('RMSE_basic_%s_horizon%s.csv'%(PID, horizon))
    MAE_basic.to_csv('MAE_basic_%s_horizon%s.csv'%(PID, horizon))
    RMSE_stacked.to_csv('RMSE_stacked_%s_horizon%s.csv'%(PID, horizon))
    MAE_stacked.to_csv('MAE_stacked_%s_horizon%s.csv'%(PID, horizon))
    
    
    # saving raw results    
    Result_SYS1.to_csv('Result_SYS1_%s_horizon%s.csv'%(PID, horizon))
    Result_SYS2.to_csv('Result_SYS2_%s_horizon%s.csv'%(PID, horizon))
    Result_SYS3.to_csv('Result_SYS3_%s_horizon%s.csv'%(PID, horizon))
    
    Result_SYS1.to_csv('Result_SYS1_%s_horizon%s.txt'%(PID, horizon))
    Result_SYS2.to_csv('Result_SYS2_%s_horizon%s.txt'%(PID, horizon))
    Result_SYS3.to_csv('Result_SYS3_%s_horizon%s.txt'%(PID, horizon))
    
    
    
    with open('dicResult_%s_horizon%s.pkl'%(PID, horizon), 'wb') as f:
        pickle.dump(d, f, protocol=2) 
        



calc_result(PID='540', horizon ='6')

