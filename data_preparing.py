"""
Created on Mar 2020

@authors: Heydar Khadem (h.khadem@sheffiel.ac.uk) and Hoda Nemat (hoda.nemat@sheffield.ac.uk)

Please cite the following work if you find this code useful for your research:
    
"Heydar Khadem, Hoda Nemat, Jackie Elliott and Mohammed Benaissa,
Multi-lag Stacking for Blood Glucose Level Prediction,
The 5th International Workshop on Knowledge Discovery in Healthcare Data,
Santiago de Compostela, Spain, August 30, 2020"

"""


import pandas as pd
from pandas import read_csv
from numpy import array
import numpy as np
from datetime import datetime
import pickle as pickle

def FindMissing_interp (seq, subset):
    
    withoutsec_str = [datetime.strptime(str(d), '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M') for d in seq.index]
    withoutsec_list = [datetime.strptime(d, '%Y-%m-%d %H:%M') for d in withoutsec_str]
    withoutsec = pd.DatetimeIndex(withoutsec_list)
 
    rounded_withoutsec = [d - pd.Timedelta(minutes=d.minute % 5) for d in withoutsec] 
    seq.index = rounded_withoutsec
    seq=seq.loc[~seq.index.duplicated(keep='first')] #keep the first value if there are more than one value in a 5 min interval

    seq_new = pd.Series(seq.values[0], index=[seq.index[0]]) #this will include missing data


    for i in range(1, len(seq)):
        
        time_delta =  pd.Timestamp(seq.index[i]) - pd.Timestamp(seq.index[i-1])

        if time_delta > pd.Timedelta('9min'): 

            seq_nan = seq.reindex(pd.date_range(start=seq.index[i-1],end=seq.index[i], freq='5min', closed='right'))
    
            seq_new=pd.concat([seq_new,seq_nan])
    
        else:
    
            real_row=pd.Series(seq.values[i],index=[seq.index[i]])
            seq_new=pd.concat([seq_new, real_row])
    
    
    nan_mask = np.isnan(seq_new) 
    
    if subset == "training":
        interpolated = seq_new.interpolate(limit_direction='both')
    if subset == "testing":
        interpolated = seq_new.interpolate(limit_direction='forward')




    return interpolated, nan_mask

def split_XY (sequence, n_steps_in, n_steps_out):
    
    nan_mask = sequence['nan_mask']
    del sequence['nan_mask']
    seq = sequence['BG']
    
    X = []
    Y = []
    X_NanMask = []
    Y_NanMask = []

    
    for i in range(len(seq)):
        
        # find the end of this pattern
        end_ix = i + n_steps_in
        out_end_ix = end_ix + n_steps_out
        # check if we are beyond the sequence
        if out_end_ix > len(sequence):
            break
        
        seq_x = seq[i:end_ix] # To use for the creation of multivar array 
        seq_y = seq[end_ix:out_end_ix]
        
        X_withNan = nan_mask[i:end_ix]
        Y_withNan = nan_mask[end_ix:out_end_ix]
    

        if Y_withNan[-1] == False:
           
           
            X.append(seq_x)
            Y.append(seq_y)
            
            
            X_NanMask.append(X_withNan)
            Y_NanMask.append(Y_withNan)
            
       
    XNanMask_array = array(X_NanMask)       
    YNanMask_array = array(Y_NanMask) 
       
    X_array = array(X)       
    Y_array = array(Y)
    
    return X_array, Y_array, XNanMask_array, YNanMask_array


###############################################################################
    ###########################################################################



def creating_DF (PID):
    
    d = {}
    subset_list=['training','testing']
    

    for k in range(len(subset_list)):
        
        d[subset_list[k]] = 'glucose_value_' + PID + '_' + subset_list[k] + '.csv'
        
        seq_BG = read_csv(d[subset_list[k]], header=0, index_col=0, parse_dates=True, squeeze=True)
        seq_glucose_value, nan_mask = FindMissing_interp(seq_BG, subset_list[k])
        BG = seq_glucose_value.values
        TS_BG = seq_glucose_value.index
        Data = pd.DataFrame({'BG': BG, 'nan_mask': nan_mask}, index=TS_BG)
        d['Data_{}'.format(subset_list[k])] = Data[-11:]
        
        if k == 0:
        
            Data_6_6 = Data[6:]
            Data_6_12 = Data[6:]
            Data_12_6 = Data[:]
            Data_12_12 = Data[:]

        if k == 1:

            frames = [d['Data_training'], Data]
            Data = pd.concat(frames)
            
            Data_6_6 = Data[12:]
            Data_6_12 = Data[6:]
            Data_12_6 = Data[6:]
            Data_12_12 = Data[:]

        #######################################################################
        Data.to_csv('%s_%s.csv' % (subset_list[k], PID))
        

        
        d['X_{}_6_6'.format(subset_list[k])], d['Y_{}_6_6'.format(subset_list[k])], d['XNanMask_{}_6_6'.format(subset_list[k])], d['YNanMask_{}_6_6'.format(subset_list[k])] = split_XY(Data_6_6, n_steps_in=6, n_steps_out=6)
        d['X_{}_6_12'.format(subset_list[k])], d['Y_{}_6_12'.format(subset_list[k])], d['XNanMask_{}_6_12'.format(subset_list[k])], d['YNanMask_{}_6_12'.format(subset_list[k])] = split_XY(Data_6_12, n_steps_in=6, n_steps_out=12)
        d['X_{}_12_6'.format(subset_list[k])], d['Y_{}_12_6'.format(subset_list[k])], d['XNanMask_{}_12_6'.format(subset_list[k])], d['YNanMask_{}_12_6'.format(subset_list[k])] = split_XY(Data_12_6, n_steps_in=12, n_steps_out=6)
        d['X_{}_12_12'.format(subset_list[k])], d['Y_{}_12_12'.format(subset_list[k])], d['XNanMask_{}_12_12'.format(subset_list[k])], d['YNanMask_{}_12_12'.format(subset_list[k])] = split_XY(Data_12_12, n_steps_in=12, n_steps_out=12)
      
    TrainTestXY_6_6 = (d['X_training_6_6'], d['Y_training_6_6'], d['X_testing_6_6'], d['Y_testing_6_6'], d['XNanMask_training_6_6'], d['YNanMask_training_6_6'], d['XNanMask_testing_6_6'], d['YNanMask_testing_6_6'])
    
    TrainTestXY_6_12 = (d['X_training_6_12'], d['Y_training_6_12'], d['X_testing_6_12'], d['Y_testing_6_12'], d['XNanMask_training_6_12'], d['YNanMask_training_6_12'], d['XNanMask_testing_6_12'], d['YNanMask_testing_6_12'])
    
    TrainTestXY_12_6 = (d['X_training_12_6'], d['Y_training_12_6'], d['X_testing_12_6'], d['Y_testing_12_6'], d['XNanMask_training_12_6'], d['YNanMask_training_12_6'], d['XNanMask_testing_12_6'], d['YNanMask_testing_12_6'])
    
    TrainTestXY_12_12 = (d['X_training_12_12'], d['Y_training_12_12'], d['X_testing_12_12'], d['Y_testing_12_12'], d['XNanMask_training_12_12'], d['YNanMask_training_12_12'], d['XNanMask_testing_12_12'], d['YNanMask_testing_12_12'])
    
   
    with open('TrainTestXY_%s_history6_horizon6.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_6_6, f, protocol=2)
        
    with open('TrainTestXY_%s_history6_horizon12.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_6_12, f, protocol=2)    
        
    with open('TrainTestXY_%s_history12_horizon6.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_12_6, f, protocol=2)

    with open('TrainTestXY_%s_history12_horizon12.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_12_12, f, protocol=2)  
        
        
        

###########################################################################



creating_DF(PID = '540')

    
    
    
    
    
    
    